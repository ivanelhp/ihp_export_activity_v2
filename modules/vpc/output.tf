output "vpc_id" {
  value = "${aws_vpc.vpc_exportv2.id}"
}

output "private_subnet_id" {
  value = "${aws_subnet.subnet_private_exportv2.id}"
}

output "public_subnet_id" {
  value = "${aws_subnet.subnet_public_exportv2.id}"
}

output "sg_webserver_id" {
  value = "${aws_security_group.sg_webserver.id}"
}

output "sg_appserver_id" {
  value = "${aws_security_group.sg_appserver.id}"
}

output "sg_dbserver_id" {
  value = "${aws_security_group.sg_dbserver.id}"
}
