# Create a VPC for export activity
resource "aws_vpc" "vpc_exportv2" {
  cidr_block = "${var.vpc_cidr}"

  tags = {
    Name = "export_vpc_v2"
  }
}

# Create a Internet Gateway
resource "aws_internet_gateway" "igw_exportv2" {
    vpc_id = "${aws_vpc.vpc_exportv2.id}"

    tags = {
      Name = "export_igw_v2"
    }
}

# Create a new Public Subnet
resource "aws_subnet" "subnet_public_exportv2" {
    vpc_id = "${aws_vpc.vpc_exportv2.id}"
    cidr_block = "${var.public_subnet_cidr}"
    map_public_ip_on_launch = "true"
    availability_zone = "${var.public_az}"

    tags = {
      Name = "export_public_subnet_v2"
    }
}

#create a new Public Route Table
resource "aws_route_table" "rt_export_v2" {
  vpc_id = "${aws_vpc.vpc_exportv2.id}"
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.igw_exportv2.id}"
  }

  tags = {
    Name = "export_rt_v2"
  }
}

# Create a new private Subnet
resource "aws_subnet" "subnet_private_exportv2" {
    vpc_id = "${aws_vpc.vpc_exportv2.id}"
    cidr_block = "${var.private_subnet_cidr}"
    availability_zone = "${var.private_az}"

    tags = {
      Name = "export_private_subnet_v2"
    }
}

# Create Security Group for webserver instances
resource "aws_security_group" "sg_webserver" {
  name = "webserver_sg"
  description = "Security Group for webservers"
  vpc_id = "${aws_vpc.vpc_exportv2.id}"

  ingress {
    from_port = "${var.ssh_port}"
    to_port = "${var.ssh_port}"
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = "${var.web_port}"
    to_port = "${var.web_port}"
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  lifecycle {
    create_before_destroy = true
  }

  tags = {
    Name = "export_sg_webserver_v2"
  }
}

# Create Security Group for appserver instances
resource "aws_security_group" "sg_appserver" {
  name = "appserver_sg"
  description = "security group for appservers"
  vpc_id = "${aws_vpc.vpc_exportv2.id}"

  ingress {
    from_port = "${var.ssh_port}"
    to_port = "${var.ssh_port}"
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = "${var.app_port}"
    to_port = "${var.app_port}"
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  lifecycle {
    create_before_destroy = true
  }

  tags = {
    Name = "export_sg_dbserver_v2"
  }
}

# Create Security Group for RDS instances
resource "aws_security_group" "sg_dbserver" {
  name = "dbserver_sg"
  description = "security group for db servers"
  vpc_id = "${aws_vpc.vpc_exportv2.id}"

  ingress {
    from_port = "${var.ssh_port}"
    to_port = "${var.ssh_port}"
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  lifecycle {
    create_before_destroy = true
  }

  tags = {
    Name = "export_sg_dbserver_v2"
  }
}
