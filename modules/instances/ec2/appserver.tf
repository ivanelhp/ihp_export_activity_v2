# Configure a remote state for VPC resources
terraform {
  backend "local" {
    path = "../remote-state/vpc/terraform.tfstate"
  }
}

resource "aws_instance" "app_server_export_v2" {
  ami = "${var.ami_appserver}"
  instance_type = "${var.inst_type_appserver}"
  key_name = "${var.ssh_key_appserver}"
  subnet_id = "${var.pvt_subnet_id}"
  vpc_security_group_ids = ["${var.app_sec_group_id}"]

  tags = {
    Name = "Appserver_exportv2"
  }
}
