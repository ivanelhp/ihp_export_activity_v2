# Configure a remote state for VPC resources
terraform {
  backend "local" {
    path = "../remote-state/vpc/terraform.tfstate"
  }
}

resource "aws_db_subnet_group" "db_subnet_group" {
  name = "export_db_subnet_groupv2"
  subnet_ids = ["${var.pvt_subnet_id}","${var.pub_subnet_id}"]
}

resource "aws_db_instance" "db_server_exportv2" {
  name = "${var.db_name}"
  username = "${var.db_user}"
  password = "${var.db_password}"
  port = "${var.db_port}"
  engine = "${var.db_engine}"
  engine_version = "${var.db_engine_version}"
  instance_class = "${var.db_instance_class}"
  allocated_storage = "${var.db_storage_size}"
  storage_type = "${var.db_storage_type}"
  identifier = "${var.db_identifier}"
  db_subnet_group_name = "${aws_db_subnet_group.db_subnet_group.name}"
  vpc_security_group_ids = ["${var.db_sec_group_id}"]
  tags = {
    Name = "dbserver_export_v2"
  }
#  vpc_security_group_ids
#  availability_zone =
}
