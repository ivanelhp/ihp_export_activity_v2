variable "ami_webserver" {}
variable "inst_type_webserver" {}
variable "ssh_key_webserver" {}
variable "web_port" {}
variable "min_webserver_size" {}
variable "max_webserver_size" {}
variable "pub_subnet_id" {}
variable "web_sec_group_id" {}
variable "vpc_id" {}
variable "public_az" {}
variable "private_az" {}
