# Create Auto Scaling Group Launch Configuration
resource "aws_launch_configuration" "lc_webserver_exportv2" {
    image_id = "${var.ami_webserver}"
    instance_type = "${var.inst_type_webserver}"
    security_groups = ["${var.web_sec_group_id}"]
    key_name = "${var.ssh_key_webserver}"
    associate_public_ip_address = "true"
    user_data = <<-EOF
                sudo yum install -y httpd
                sudo sed -i 's/Listen 80/Listen 8080/' /etc/httpd/conf/httpd.conf
                sudo systemctl enable httpd.service
                sudo systemctl start httpd.service
    EOF

    lifecycle {
      create_before_destroy = true
    }
}

# Create Auto Scaling Group
resource "aws_autoscaling_group" "asg_webserver" {
    launch_configuration = "${aws_launch_configuration.lc_webserver_exportv2.name}"
    vpc_zone_identifier = ["${var.pub_subnet_id}"]
    availability_zones = ["${var.public_az}","${var.private_az}"]
    min_size = "${var.min_webserver_size}"
    max_size = "${var.max_webserver_size}"
    load_balancers = ["${aws_elb.elb_webserver.name}"]
    health_check_type = "ELB"
}

resource "aws_elb" "elb_webserver" {
    name = "elb-export-activity"
    subnets = ["${var.pub_subnet_id}"]
    security_groups = ["${var.web_sec_group_id}"]
#    availability_zones =["${var.public_az}","${var.private_az}"]

    health_check {
      healthy_threshold = 2
      unhealthy_threshold = 2
      timeout = 3
      target = "HTTP:${var.web_port}/"
      interval = 30
    }

    listener {
      instance_port = "${var.web_port}"
      instance_protocol = "http"
      lb_port = "${var.web_port}"
      lb_protocol = "http"
    }

    tags = {
      Name = "export_elb_v2"
    }
}
# Create a Security Group for Elastic Load Balancer
resource "aws_security_group" "sg_elb" {
    name = "elb_sg"
    vpc_id = "${var.vpc_id}"
    egress {
      from_port = 0
      to_port = 0
      protocol = "-1"
      cidr_blocks = ["0.0.0.0/0"]
    }

    ingress {
      from_port = "${var.web_port}"
      to_port = "${var.web_port}"
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }

    tags = {
      Name = "export_sg_elb_v2"
    }
}
