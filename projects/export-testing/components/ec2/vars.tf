# Variables for aws provider
variable aws_region {}
variable aws_access_key {}
variable aws_secret_key {}

# Variables for EC2 resources
variable "ami_appserver" {}
variable "inst_type_appserver" {}
variable "ssh_key_appserver" {}
variable "pvt_subnet_id" {
  default = ""
}
variable "app_sec_group_id" {
  default = ""
}
