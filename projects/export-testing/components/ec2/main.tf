data "terraform_remote_state" "vpc" {
  backend = "local"
  config = {
    path = "../../remote-state/vpc/terraform.tfstate"
  }
}

module "appserver_instance" {
  source = "../../../../modules/instances/ec2/"
  ami_appserver = "${var.ami_appserver}"
  inst_type_appserver = "${var.inst_type_appserver}"
  ssh_key_appserver = "${var.ssh_key_appserver}"
  pvt_subnet_id = "${data.terraform_remote_state.vpc.outputs.private_subnet_id}"
  app_sec_group_id = "${data.terraform_remote_state.vpc.outputs.sg_appserver_id}"
}
