# Variables for aws provider
variable aws_region {}
variable aws_access_key {}
variable aws_secret_key {}

# Variables for RDS resources
variable "db_name" {}
variable "db_user" {}
variable "db_password" {}
variable "db_port" {}
variable "db_engine" {}
variable "db_engine_version" {}
variable "db_storage_type" {}
variable "db_storage_size" {}
variable "db_instance_class" {}
variable "db_identifier" {}
variable "pvt_subnet_id" {
  default = ""
}
variable "pub_subnet_id" {
  default = ""
}
variable "db_sec_group_id" {
  default = ""
}
