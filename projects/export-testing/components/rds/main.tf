data "terraform_remote_state" "vpc" {
  backend = "local"
  config = {
    path = "../../remote-state/vpc/terraform.tfstate"
  }
}

module "dbserver_instance" {
  source = "../../../../modules/instances/rds"
  db_name = "${var.db_name}"
  db_user = "${var.db_user}"
  db_password = "${var.db_password}"
  db_port = "${var.db_port}"
  db_engine = "${var.db_engine}"
  db_engine_version = "${var.db_engine_version}"
  db_instance_class = "${var.db_instance_class}"
  db_storage_size = "${var.db_storage_size}"
  db_storage_type = "${var.db_storage_type}"
  db_identifier = "${var.db_identifier}"
  pvt_subnet_id = "${data.terraform_remote_state.vpc.outputs.private_subnet_id}"
  pub_subnet_id = "${data.terraform_remote_state.vpc.outputs.public_subnet_id}"
  db_sec_group_id = "${data.terraform_remote_state.vpc.outputs.sg_dbserver_id}"
}
