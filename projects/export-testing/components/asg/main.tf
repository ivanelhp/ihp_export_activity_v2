data "terraform_remote_state" "vpc" {
  backend = "local"
  config = {
    path = "../../remote-state/vpc/terraform.tfstate"
  }
}

module "asg_web_server" {
  source = "../../../../modules/autoscaling/"
  ami_webserver = "${var.ami_webserver}"
  inst_type_webserver = "${var.inst_type_webserver}"
  ssh_key_webserver = "${var.ssh_key_webserver}"
  web_port = "${var.web_port}"
  min_webserver_size = "${var.min_webserver_size}"
  max_webserver_size = "${var.max_webserver_size}"
  pub_subnet_id = "${data.terraform_remote_state.vpc.outputs.public_subnet_id}"
  web_sec_group_id = "${data.terraform_remote_state.vpc.outputs.sg_webserver_id}"
  vpc_id =  "${data.terraform_remote_state.vpc.outputs.vpc_id}"
  public_az = "${var.public_az}"
  private_az = "${var.private_az}"
}
