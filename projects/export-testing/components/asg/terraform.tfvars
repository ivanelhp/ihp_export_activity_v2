aws_region = "us-east-1"
aws_access_key = "put your access key here"
aws_secret_key = "put your secret key here"


#Values for Auto Scaling Group
ami_webserver = "ami-0c6b1d09930fac512"
inst_type_webserver = "t2.nano"
ssh_key_webserver = "MyActivityKeyPair"
web_port = "8080"
min_webserver_size = "2"
max_webserver_size = "4"
public_az = "us-east-1a"
private_az = "us-east-1c"
