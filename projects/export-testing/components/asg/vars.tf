# Variables for aws provider
variable aws_region {}
variable aws_access_key {}
variable aws_secret_key {}

# Variables for Auto Scaling Group resources
variable "ami_webserver" {}
variable "inst_type_webserver" {}
variable "ssh_key_webserver" {}
variable "web_port" {}
variable "vpc_id" {
  default = ""
}
variable "max_webserver_size" {}
variable "min_webserver_size" {}
variable "public_az" {}
variable "private_az" {}
variable "pub_subnet_id" {
  default = ""
}
variable "web_sec_group_id" {
  default = ""
}
