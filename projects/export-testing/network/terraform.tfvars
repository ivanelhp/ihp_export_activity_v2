# Values for provider
aws_region = "us-east-1"
aws_access_key = "put your access key here"
aws_secret_key = "put your secret key here"


# Values for VPC Resources
vpc_cidr = "10.0.0.0/16"
public_az = "us-east-1a"
private_az = "us-east-1c"
public_subnet_cidr = "10.0.1.0/24"
private_subnet_cidr = "10.0.2.0/24"
web_port = "8080"
app_port = "9043"
db_port = "9043"
ssh_port = "22"
