# Variables for aws provider
variable aws_region {}
variable aws_access_key {}
variable aws_secret_key {}

# Variables for VPC resources
variable "vpc_cidr" {}
variable "public_az" {}
variable "private_az" {}
variable "public_subnet_cidr" {}
variable "private_subnet_cidr" {}
variable "app_port" {}
variable "db_port" {}
variable "ssh_port" {}
variable "web_port" {}
