output "vpc_id" {
  value = "${module.network.vpc_id}"
}

output "private_subnet_id" {
  value = "${module.network.private_subnet_id}"
}

output "public_subnet_id" {
  value = "${module.network.public_subnet_id}"
}

output "sg_webserver_id" {
  value = "${module.network.sg_webserver_id}"
}

output "sg_appserver_id" {
  value = "${module.network.sg_appserver_id}"
}

output "sg_dbserver_id" {
  value = "${module.network.sg_dbserver_id}"
}
