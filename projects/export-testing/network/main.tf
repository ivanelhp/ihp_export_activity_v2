# Configure a remote state for VPC resources
terraform {
  backend "local" {
    path = "../remote-state/vpc/terraform.tfstate"
  }
}


module "network" {
  source = "../../../modules/vpc/"
  vpc_cidr = "${var.vpc_cidr}"
  public_az = "${var.public_az}"
  private_az = "${var.private_az}"
  public_subnet_cidr = "${var.public_subnet_cidr}"
  private_subnet_cidr = "${var.private_subnet_cidr}"
  app_port = "${var.app_port}"
  db_port = "${var.db_port}"
  ssh_port = "${var.ssh_port}"
  web_port = "${var.web_port}"
}
